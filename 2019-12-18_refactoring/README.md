# RULES

- work in pairs
- switch pairs every 30 minutes
- to generate the pairs: https://www.randomlists.com/team-generator

# AGENDA

1. TennisGame
2. Do more katas on https://kata-log.rocks/refactoring
  - begin w/https://kata-log.rocks/bugs-zero-kata
3. Vue
  refactor src/core/util/options.js
