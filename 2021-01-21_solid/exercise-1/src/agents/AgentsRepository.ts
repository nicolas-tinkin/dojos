import { Repository } from "../standars/repositories/Repository";
import { Agent } from "./Agent";

export interface AgentsRepository extends Repository<Agent, string> {
}
