import {BackedMissionsService} from "../../src/missions/backedMissions/BackedMissionsService";
import { Agent } from "../../src/agents/Agent";
import { BackedMission } from "../../src/missions/backedMissions/BackedMission";
import { BackedMissionsRepository } from "../../src/missions/backedMissions/BackedMissionsRepository";
import { InMemoryBackedMissionsRepository } from "../../src/missions/backedMissions/InMemoryBackedMissionsRepository";
import {InvalidMission} from "../../src/missions/errors/InvalidMission";

describe("backed missions", () => {
    let backedMissionRepository: BackedMissionsRepository;
    let backedMissionService: BackedMissionsService;

    let backedMissions: BackedMission[];

    let solidSnake: Agent;
    let merylSilverburgh: Agent;
    let shadowMosesMission: BackedMission;

    beforeEach(async () => {
        solidSnake = new Agent("solid-snake");
        merylSilverburgh = new Agent("meryl-silverburgh");
        shadowMosesMission = new BackedMission(
            "shadow-moses-island",
            "Shadow Moses Island",
            solidSnake,
            [merylSilverburgh],
        );

        backedMissions = [
            shadowMosesMission,
        ];

        backedMissionRepository = new InMemoryBackedMissionsRepository();
        backedMissionService = new BackedMissionsService(backedMissionRepository);
        backedMissions.forEach(async (mission) => await backedMissionService.addMission(mission));
    });

    it("provides the list of all backed missions", async () => {
        expect(await backedMissionService.getAllMissions()).toEqual(backedMissions);
    });

    it("provides the mission of an agent when he is a backup", async () => {
        expect(await backedMissionService.getAgentMissions("meryl-silverburgh")).toContain(shadowMosesMission);
    });

    it("does be able to remove a backup from a mission", async () => {
        await backedMissionService.removeBackupFromMission(shadowMosesMission, "meryl-silverburgh");

        const missionInformation: BackedMission | undefined
            = await backedMissionService.getBackedMissionInformation("shadow-moses-island");

        expect(missionInformation && missionInformation.getBackup()).not.toContain(merylSilverburgh);
    });

    it("doesn't be able to add a backed mission without a backup", async () => {
        const tselinoyarskMission = new BackedMission(
            "",
            "Tselinoyarsk",
            solidSnake,
            [],
        );

        await expect(backedMissionService.addBackedMission(tselinoyarskMission))
        .rejects.toEqual(new InvalidMission(tselinoyarskMission));
    })
});
