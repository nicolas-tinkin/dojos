import { BackedMission } from "./backedMissions/BackedMission";
import { isAgentInBackup } from "./backedMissions/BackedMissionsHelpers";
import { BackedMissionsRepository } from "./backedMissions/BackedMissionsRepository";
import { InvalidMission } from "./errors/InvalidMission";
import { MissionConflict } from "./errors/MissionConflicts";
import { InMemoryMissionsRepository } from "./InMemoryMissionsRepository";
import { Mission } from "./Mission";
import { hasAlreadyAMissionWithinThisPeriod, isMissionValid } from "./MissionPolicies";
import { getMissionWithinPeriod } from "./MissionsHelper";

export class MissionsService {
    private missionsRepository: InMemoryMissionsRepository;
    private backedMissionsRepository: BackedMissionsRepository;

    constructor(missionsRepository: InMemoryMissionsRepository, backedMissionsRepository: BackedMissionsRepository) {
        this.missionsRepository = missionsRepository;
        this.backedMissionsRepository = backedMissionsRepository;
    }

    public async addMission(mission: Mission): Promise<boolean> {
        if (!isMissionValid(mission)) {
            throw new InvalidMission(mission);
        }

        if (hasAlreadyAMissionWithinThisPeriod(
            await this.getAgentMissions(mission.getAgent().getId()),
            mission,
        )) {
            throw new MissionConflict(mission);
        }

        return await this.missionsRepository.add(mission);
    }

    public async getAllMissions(): Promise<Mission[]> {
        return await this.missionsRepository.findAll();
    }

    public async getMissionInformation(missionId: string): Promise<Mission | undefined> {
        return await this.missionsRepository.findById(missionId);
    }

    public async getAgentMissions(agentId: string): Promise<Mission[]> {
        return await this.missionsRepository.findByAgent(agentId);
    }

    public async getAgentCurrentMission(agentId: string): Promise<Mission | undefined> {
        const currentDate = (new Date()).getTime();
        return getMissionWithinPeriod(await this.getAgentMissions(agentId), currentDate, currentDate);
    }

    public async getAllBackedMissions(): Promise<Mission[]> {
        return await this.backedMissionsRepository.findAll();
    }

    public async getAgentBackedMissions(agentId: string): Promise<Mission[]> {
        return await this.backedMissionsRepository.findByAgent(agentId);
    }

    public async getBackedMissionInformation(missionId: string): Promise<BackedMission | undefined> {
        return await this.backedMissionsRepository.findById(missionId);
    }

    public async removeBackupFromMission(mission: BackedMission, backupId: string): Promise<boolean> {
        if (!isAgentInBackup(backupId, mission)) {
            return false;
        }

        return this.backedMissionsRepository.removeBackup(mission.getId(), backupId);
    }
}
