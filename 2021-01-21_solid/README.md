# Coding Dojo - SOLID Principles

A Coding Dojo based on the universe of Metal Gear Solid. Five exercises will be presented to introduce the principles behind the SOLID acronym.

# Introduction

Transmission received...

```
Snake, we need your help.
Our agent management system has been compromised and we are trying to rebuild another one in Node.js.
The system is used to keep track of all of our agents and their missions. No big deal for a clean coder like you, am I right?
We are almost done, but something is missing, like if we were building the project without thinking of the maintainability, flexibility and understandability...
Your goal is to find the project's flaws and eliminate them. Our first analysis has shown that there is one of them per principle behind the SOLID acronym, whatever it means.
```

* Follow the instructions and try to complete the exercises.

# Dojo

## Single Responsability Principle

Transmission received...

```
Snake, we have found your first target.
This is a common case of a junk room class! Clean it by making sure that the responsibilities are at the right place.
```

### Exercise

In the folder `exercise-1`.

Try to clean up the `addMission` in the `MissionService` by splitting the responsibilities in different classes.

## Open/Closed Principle

Transmission received...

```
Well done with the previous mess !
Now, if we want to be able to add more specificities to the project, we will need to rethink our classes strategy.
```

### Exercise

In the folder `exercise-2`.

We have add a new feature, the missions with backup. These are just an extension of the previous missions, but with a list of agents that can be used as backup.

Unfortunately, we rushed the feature and now with have a messy `MissionsService`.

Try to clean it by putting all of the `BackedMission` specifics in an extension of the `MissionsService`.

## Liskov Substitution Principle

Transmission received...

```
Still with us? We need you more than ever with this issue.
The project act oddly depending on the implementation, no matter how precise the API is..
Spot the weak substitution and clear it, so we can move on.
```

### Exercise

In the folder `exercise-3`.

The team has decided to add a new condition for creating a mission with backup.

This condition is that there must be at least one of those backups.

The first implementation seems to fulfill this requirement, adding a `BackedMission` with no backup throws an `InvalidMission` exception.

But the same entity doest not behave the same while being added in the `MissionsService` or in the `BackedMissionsService`. Thus, we cannot substitue those classes, even if `BackedMissionsService` extends `MissionsService`.

## Interface Segregation Principle

Transmission received...

```
The previous mission was the most difficult, keep on cleaning!

A lot of integrators expressed their frustration about our API. They keep telling us that there is way too much useless things to implement, even unnecessary methods...
```

### Exercise

In the folder `exercise-4`.

Our engineers are working on a new feature, the possibility to manage the information of the missions’ locations.

The data will be fetched from a Repository, but they need to be able to delete elements from this repository.

To do so, they have started to add a new delete method.

```javascript
export interface Repository<T, K> {
    add(element: T): Promise<boolean>;
    findAll(): Promise<T[]>;
    findById(id: K): Promise<T | undefined>;
    delete(id: k): Promise<boolean>;
}
```

As you can imagine, the compiler indicates a lot of error based on this new addition, exemple:

```
Property 'delete' is missing in type 'InMemoryMissionsRepository' but required in type 'MissionsRepository'
```

It might be due to the fact that the interface holds to many methods!

Split this interface to best suit the new needs of the team.

## Dependency Inversion Principle

Transmission received...

```
One last thing to greatly improve our project and you will be free to go.

The team wants to be able to try numerous technical choices before deciding which one to use in production.

To do so, we need to rethink our application to remove any explicit references!
```

### Exercise

In the folder `exercise-5`.

The `AgentsService` make explicit reference to an implementation of the `AgentsRepository`. Abstract the used repository.

```javascript
export class AgentsService {
    private agentsRepository: InMemoryAgentsRepository;
    constructor(agentsRepository: InMemoryAgentsRepository) {
        this.agentsRepository = agentsRepository;
    }
    // [...]
}
```

The only thing that the service should know is that the repository can perform various actions.

# Conclusion

```
Great job Snake, you have managed to tremendously clear the project!
```
