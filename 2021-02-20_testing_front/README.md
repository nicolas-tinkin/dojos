# Dojo - Testing en React

## Introducción

En el siguiente dojo encontrarás un bundle de React con lo necesario para realizar los siguientes ejercicios:

### Ejercicio 1

Dentro de la carpeta `src/components/` encontrarás un componente llamado `ButtonComponent`, el cual recibe 3 propiedades, `buttonText` que es el texto a mostrar dentro del botón, `isDisabled` que es un booleano que dice si el botón debe estar deshabilitado o no y `onClickButton` que es la acción que se va a disparar cuando se presione el botón.

El componente se realizó sin la técnica de `TDD` y solamente cuenta con un test báscio. Para poder crear nuevas funcionalidades se requiere que se prueben los siguientes casos de uso:

* Que se muestre en el botón el texto pasado por la propiedad `buttonText`.
* Que cuando se haga click en el botón se llame a la propiedad `onClickButton`.
* Que esté el botón disabled cuando recibe la propiedad `isDisabled`.

### Ejercicio 2

Dentro de la carpeta `src/components` encontrarás un componente llamado `InputComponent`, el cual recibe 3 propiedades, `value` que es el valor que se muestra en el input, `onChangeInputValue` que es la accêno que se va a llamar cuando se cambie un valor del input y `error` que es un mensaje de error.

De la misma forma el componente se realizó sin la técnica de `TDD` y solamente cuenta con un test básico, para poder crear nuevas funcionalidades se requiere que se prueben los siguientes casos de uso:

* Se debe mostrar el valor pasado por la propiedad `value` en el input.
* Se debe llamar a la acción `onChangeInputValue` cuando se cambie una valor dentro del input.
* Se debe muestrar el valor de `error` cuando se pase por esta propiedad.

### Ejercicio 3

En el archivo `App.js` en la carpeta `src` se encuentra la página de inicio, en la cual está el `ButtonComponent` y el `InputComponent` previamente mencionados, se van a realizar cambios sobre esta página, por lo que se requiere realizar tests para asegurarse que no se pierdan las funcionalidades ya realizadas.

Se tienen que testear los siguientes casos de uso:

* Se debe renderizar el `InputComponent` correctamente.
* Se debe renderizar el `ButtonComponent` correctamente.
* Se debe habilitar el `ButtonComponent` para que se pueda clickear una vez se llene el input.
* Se debe mostrar un mensaje: `El formulario se ha enviado correctamente con el valor ${value_del_input}` cuando se llene el input y se dé click en el botón. Como se puede ver en el mensaje se debe pasar el valor del input.

**Nota**: La página `App.js` no cuenta con una archivo de test, por lo cual debes crearlo en la carpeta `tests`;
