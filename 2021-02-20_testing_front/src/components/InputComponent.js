import * as React from 'react'

const InputComponent = (props) => {
  const { value, error, onChangeInputValue } = props;

  const handleChangeInputValue = (e) => {
    onChangeInputValue(e.target.value);
  }

  return (
    <div className="input-component">
      <div>Input Component</div>
      <input onChange={handleChangeInputValue} value={value} />
      {error && <div>{error}</div>}
    </div>
  )
}

export default InputComponent
