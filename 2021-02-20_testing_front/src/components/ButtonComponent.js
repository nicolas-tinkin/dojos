import * as React from 'react';

const ButtonComponent = (props) => {
  const { buttonText, onClickButton, isDisabled } = props;
  return (
    <button className="button-component" onClick={onClickButton} disabled={isDisabled}>{buttonText}</button>
  );
}

export default ButtonComponent;
