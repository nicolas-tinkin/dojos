import { render } from '@testing-library/react'
import '@testing-library/jest-dom/extend-expect'
import InputComponent from '../../components/InputComponent';

describe('InputComponent', () => {
  describe('when render with default props', () => {
    it('returns first element with className equal to component name', () => {
      const { container } = render(<InputComponent />)
      expect(container.children[0].className).toContain('input-component')
    });
  });
});
