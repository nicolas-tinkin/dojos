import {render} from "@testing-library/react";
import ButtonComponent from "../../components/ButtonComponent";

describe('ButtonComponent', () => {
  describe('when receive default props', () => {
    it('returns first element with className equal to component name', () => {
      const { container } = render(<ButtonComponent />)
      expect(container.children[0].className).toContain('button-component')
    });
  });
});
