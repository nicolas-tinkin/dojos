import * as React from 'react';
import ButtonComponent from './components/ButtonComponent';
import InputComponent from './components/InputComponent';

function App() {
  const [inputValue, changeInputValue] = React.useState('');
  const [alertMessage, changeAlertMessage] = React.useState('');

  const handleChangeAlertMessage = () => {
    changeAlertMessage(`El formulario se ha enviado correctamente con el valor ${alertMessage}`);
  }
  return (
    <div className="App">
      <InputComponent value={inputValue} onChangeInputValue={changeInputValue} />
      <ButtonComponent buttonText="Subir formulario" onClickButton={handleChangeAlertMessage} isDisabled={!inputValue} />
      {alertMessage && <div>{alertMessage}</div>}
    </div>
  );
}

export default App;
